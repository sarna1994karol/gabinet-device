#DHCP + SAMBA
sudo apt-get --assume-yes install isc-dhcp-server --fix-missing >> install.log
sudo apt-get --assume-yes install winbind --fix-missing >> install.log
sudo echo "GABINETPC" > /etc/hostname

#VNC
#yum install --assume-yes vnc vnc-server --fix-missing >> install.log

#SSH
#sudo apt-get --assume-yes install openssh-server --fix-missing >> install.log
#sudo apt-get --assume-yes install openssh-client --fix-missing >> install.log

#Gabinet
sudo apt-get --assume-yes install postgresql postgresql-contrib --fix-missing >> install.log
sudo apt-get --assume-yes install python --fix-missing >> install.log
sudo apt-get --assume-yes install python-dev --fix-missing >> install.log
sudo apt-get --assume-yes install python-django --fix-missing >> install.log
sudo apt-get --assume-yes build-dep python-psycopg2 --fix-missing >> install.log
sudo apt-get --assume-yes install python-pip --fix-missing >> install.log
sudo apt-get --assume-yes install libjpeg-dev --fix-missing >> install.log
sudo apt-get --assume-yes install libpq-dev --fix-missing >> install.log
sudo apt-get --assume-yes install subversion --fix-missing >> install.log
sudo apt-get --assume-yes install ruby-full  --fix-missing >> install.log
sudo apt-get --assume-yes install libxml2-dev  --fix-missing >> install.log
sudo apt-get --assume-yes install libxslt-dev  --fix-missing >> install.log
sudo gem install sass >> install.log

sudo pip install virtualenv >> install.log

cd ../Gabinet
svn checkout svn://k.sarnecki@10.0.0.90/Gabinet >> install.log

sudo runuser postgres -c "echo \"create database gabinet_remote_drugs\" | psql" >> install.log
sudo runuser postgres -c  "echo \"create database gabinet_local\"| psql" >> install.log
sudo runuser postgres -c "echo \"alter user postgres with password 'xxx'\" | psql "
#TODO => sudo echo "local	all	postgres		md5" >> /etc/postgresql/9.4/main/pg_hba.conf

virtualenv venv
source venv/bin/activate
cd Gabinet/django
pip install -r pip_essentials.txt >> install.log
pip install --upgrade django==1.3 >> install.log
pip install psycopg2 >> install.log
pip install django-realtime --upgrade >> install.log
pip install django-countries >> install.log
pip install pil >> install.log
pip install image >> install.log
sudo pip install xlwt >> install.log
pip install django-webodt >> install.log
sudo pip install oauthlib  >> install.log
sudo pip install python-dateutil --upgrade >> install.log

python manage.py makemigrations 
python manage.py migrate
#./update_remote_drugs
sass static/css/style-main.scss:static/css/style-main.css

cd ../../CppDaemon
sudo echo $(pwd)/gabinetService > /etc/rc.local
sudo echo "exit 0" >> /etc/rc.local

sudo cp smb.conf  /etc/samba/
sudo cp nsswitch.conf /etc/

sudo reboot
