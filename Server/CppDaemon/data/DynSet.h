
#ifndef _LK_DYNSET_H_
#define _LK_DYNSET_H_

template <class T>
class DSBox {
  T* t;
public:
  DSBox();
  DSBox(const T&);
  virtual DSBox& operator=(const DSBox&); 
  virtual T& getValue();
  virtual const T& getValue() const;
  virtual DSBox<T>* clone() const;
  virtual ~DSBox();
};

template <class T>
class DynSet {
  int len;
  int size;
protected:
  typedef DSBox<T>* DSBoxPtr;
  DSBoxPtr* buf;
  void Free();
  void UpSize();
public:
  DynSet();
  DynSet(const DynSet<T>&);
  virtual DynSet<T>& operator=(const DynSet<T>&);
  virtual DynSet<T>& operator+=(const DynSet<T>&);

  virtual void Insert(const T&);
  virtual const T& operator[](int) const;
  virtual T& operator[](int);
  virtual int Size() const;
  virtual void Delete(int);

  virtual ~DynSet();
};

template <class T>
class UniqueDynSet : public DynSet<T> {
public:
  UniqueDynSet();
  UniqueDynSet(const DynSet<T>&);

  virtual UniqueDynSet<T> Unique() const;
  virtual ~UniqueDynSet();
};

#include "DynSet.cpp"

#endif
