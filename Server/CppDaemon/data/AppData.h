#ifndef _APPDATA_GEN_H_
#define _APPDATA_GEN_H_
#include "DynSet.h"
//------------- string ---------------
#include "AnsiString.h"
//----------------------------------

//------------- int ---------------
//----------------------------------

//------------- Bool ---------------
class Bool {
  int _type;
  void* _ptr;

  static const int _TypeTrue;
  static const int _TypeFalse;

  virtual void init(int, void*);
  virtual void clean();
  Bool();
public:
  Bool(const Bool&);
  virtual Bool& operator=(const Bool&);

  virtual bool isTrue() const;
  virtual bool isFalse() const;



  virtual ~Bool();

  static Bool createTrue();
  static Bool createFalse();

};
//----------------------------------

//------------- DhcpStatus ---------------
class DhcpStatus {
  int _type;
  void* _ptr;

  static const int _TypeEmpty;
  static const int _TypeDhcpDetected;
  static const int _TypeNoDhcpDetected;
  static const int _TypeDhcpRun;

  virtual void init(int, void*);
  virtual void clean();
  DhcpStatus();
public:
  DhcpStatus(const DhcpStatus&);
  virtual DhcpStatus& operator=(const DhcpStatus&);

  virtual bool isEmpty() const;
  virtual bool isDhcpDetected() const;
  virtual bool isNoDhcpDetected() const;
  virtual bool isDhcpRun() const;

  virtual const AnsiString& asDhcpDetected() const;
  virtual AnsiString& asDhcpDetected();


  virtual ~DhcpStatus();

  static DhcpStatus createEmpty();
  static DhcpStatus createDhcpDetected(const AnsiString&);
  static DhcpStatus createNoDhcpDetected();
  static DhcpStatus createDhcpRun();

};
//----------------------------------

//------------- IfaceStatus ---------------
class IfaceStatus {
  Bool plugIn;
  AnsiString ipv4;
  DhcpStatus dhcpStatus;
  Bool pingOk;
public:
  IfaceStatus(const Bool&, const AnsiString&, const DhcpStatus&, const Bool&);
  virtual const Bool& getPlugIn() const;
  virtual const AnsiString& getIpv4() const;
  virtual const DhcpStatus& getDhcpStatus() const;
  virtual const Bool& getPingOk() const;
  virtual Bool& getPlugIn();
  virtual AnsiString& getIpv4();
  virtual DhcpStatus& getDhcpStatus();
  virtual Bool& getPingOk();


  virtual ~IfaceStatus();

};
//----------------------------------

//------------- WifiNetwork ---------------
class WifiNetwork {
  AnsiString ssid;
  Bool connected;
public:
  WifiNetwork(const AnsiString&, const Bool&);
  virtual const AnsiString& getSsid() const;
  virtual const Bool& getConnected() const;
  virtual AnsiString& getSsid();
  virtual Bool& getConnected();


  virtual ~WifiNetwork();

};
//----------------------------------

//------------- WifiList ---------------
#include "DynSet.h"


class WifiList : public DynSet<WifiNetwork> {
public:
  WifiList();


  virtual ~WifiList();

};
//----------------------------------

//------------- NetStatus ---------------
class NetStatus {
  IfaceStatus eth;
  IfaceStatus wlan;
  IfaceStatus lte;
  WifiList wifiList;
public:
  NetStatus(const IfaceStatus&, const IfaceStatus&, const IfaceStatus&, const WifiList&);
  virtual const IfaceStatus& getEth() const;
  virtual const IfaceStatus& getWlan() const;
  virtual const IfaceStatus& getLte() const;
  virtual const WifiList& getWifiList() const;
  virtual IfaceStatus& getEth();
  virtual IfaceStatus& getWlan();
  virtual IfaceStatus& getLte();
  virtual WifiList& getWifiList();


  virtual ~NetStatus();

};
//----------------------------------

//------------- Data ---------------
class Data {
  NetStatus netStatus;
public:
  Data(const NetStatus&);
  virtual const NetStatus& getNetStatus() const;
  virtual NetStatus& getNetStatus();


  virtual ~Data();

};
//----------------------------------

#endif
