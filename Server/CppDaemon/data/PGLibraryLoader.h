//---------------------------------------------------------------------------

#ifndef PGLibraryLoaderH
#define PGLibraryLoaderH
//---------------------------------------------------------------------------

#include "AnsiString.h"

#ifndef NOPGSQL

#ifdef __GNUC__

#include <libpq-fe.h>

#else

typedef enum {
  CONNECTION_OK,
  CONNECTION_BAD,
  CONNECTION_STARTED,
  CONNECTION_MADE,
  CONNECTION_AWAITING_RESPONSE,
  CONNECTION_AUTH_OK,
  CONNECTION_SETENV
} ConnStatusType;

typedef enum
{
  PGRES_EMPTY_QUERY = 0,
  PGRES_COMMAND_OK,			/* a query command that doesn't return
								 * anything was executed properly by the
								 * backend */
  PGRES_TUPLES_OK,			/* a query command that returns tuples was
								 * executed properly by the backend,
								 * PGresult contains the result tuples */
  PGRES_COPY_OUT,				/* Copy Out data transfer in progress */
  PGRES_COPY_IN,				/* Copy In data transfer in progress */
  PGRES_BAD_RESPONSE,			/* an unexpected response was recv'd from
								 * the backend */
  PGRES_NONFATAL_ERROR,
  PGRES_FATAL_ERROR
} ExecStatusType;

typedef struct pg_conn PGconn;
typedef struct pg_result PGresult;
typedef PGconn *tPQconnectdb(const char *conninfo);
typedef void tPQfinish(PGconn *conn);
typedef ConnStatusType tPQstatus(const PGconn *conn);
typedef char *tPQerrorMessage(const PGconn *conn);
typedef PGresult *tPQexec(PGconn *conn, const char *query);
typedef int tPQntuples(const PGresult *res);
typedef int tPQnfields(const PGresult *res);
typedef char* tPQgetvalue(const PGresult *res, int tup_num, int field_num);
typedef ExecStatusType tPQresultStatus(const PGresult *res);
typedef char *tPQresultErrorMessage(const PGresult *res);
typedef void tPQclear(PGresult *res);

#endif

#else

typedef enum {
  CONNECTION_OK,
  CONNECTION_BAD,
  CONNECTION_STARTED,
  CONNECTION_MADE,
  CONNECTION_AWAITING_RESPONSE,
  CONNECTION_AUTH_OK,
  CONNECTION_SETENV
} ConnStatusType;

typedef enum
{
  PGRES_EMPTY_QUERY = 0,
  PGRES_COMMAND_OK,			/* a query command that doesn't return
								 * anything was executed properly by the
								 * backend */
  PGRES_TUPLES_OK,			/* a query command that returns tuples was
								 * executed properly by the backend,
								 * PGresult contains the result tuples */
  PGRES_COPY_OUT,				/* Copy Out data transfer in progress */
  PGRES_COPY_IN,				/* Copy In data transfer in progress */
  PGRES_BAD_RESPONSE,			/* an unexpected response was recv'd from
								 * the backend */
  PGRES_NONFATAL_ERROR,
  PGRES_FATAL_ERROR
} ExecStatusType;

typedef void PGconn;
typedef void PGresult;

#endif

class PGLibraryLoader {

#ifndef NOPGSQL

#ifdef __GNUC__
#else
  tPQconnectdb* PQconnectdb;
  tPQfinish* PQfinish;
  tPQstatus* PQstatus;
  tPQerrorMessage* PQerrorMessage;
  tPQexec* PQexec;
  tPQntuples* PQntuples;
  tPQnfields* PQnfields;
  tPQgetvalue* PQgetvalue;
  tPQresultStatus* PQresultStatus;
  tPQresultErrorMessage* PQresultErrorMessage;
  tPQclear* PQclear;
#endif

#endif

public:
  PGLibraryLoader(const AnsiString&);

  virtual PGconn* connectdb(const AnsiString& conninfo);
  virtual void finish(PGconn *conn);
  virtual ConnStatusType status(const PGconn *conn);
  virtual AnsiString errorMessage(const PGconn *conn);
  virtual PGresult *exec(PGconn *conn, const AnsiString& query);
  virtual int ntuples(const PGresult *res);
  virtual int nfields(const PGresult *res);
  virtual AnsiString getvalue(const PGresult *res, int tup_num, int field_num);
  virtual ExecStatusType resultStatus(const PGresult *res);
  virtual AnsiString resultErrorMessage(const PGresult *res);
  virtual void clear(PGresult *res);

  virtual ~PGLibraryLoader();
};

#endif
