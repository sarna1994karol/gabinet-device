//---------------------------------------------------------------------------

#ifndef DBIntfH
#define DBIntfH
//---------------------------------------------------------------------------

#include "AnsiString.h"
#include "DynSet.h"

class SDBResult : public DynSet< DynSet<AnsiString> > {
  friend class SDBConn;
public:
  virtual ~SDBResult();
};

class DBIntf {
public:
  virtual void Connect(const AnsiString&, const AnsiString&, const AnsiString&, const AnsiString&) = 0;
  virtual void Execute(const AnsiString&) = 0;
  virtual SDBResult Query(const AnsiString&) = 0;
  virtual void Disconnect() = 0;
  virtual AnsiString Escape(const AnsiString&) = 0;
  virtual ~DBIntf();
};

#endif
