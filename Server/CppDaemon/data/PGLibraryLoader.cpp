//---------------------------------------------------------------------------

#include "PGLibraryLoader.h"

#ifndef NOPGSQL

#ifdef __GNUC__

PGLibraryLoader::PGLibraryLoader(const AnsiString&) {
}                      

#else
//---------------------------------------------------------------------------

FARPROC safeReadSymbol(HINSTANCE handle, const AnsiString& symbol) {
  FARPROC proc = GetProcAddress(handle, symbol.c_str());
  if (proc==0)
    throw Exception("Nie mo�na odczyta� wymaganego symbolu z biblioteki ["+symbol+"].");
  return proc;
}

PGLibraryLoader::PGLibraryLoader(const AnsiString& file) {
  HINSTANCE handle = LoadLibrary(file.c_str());
  if (handle==0)
    throw Exception("Nie mo�na otworzy� wymaganego pliku ["+file+"].");
  FARPROC procConnect = safeReadSymbol(handle, "PQconnectdb");
  FARPROC procFinish = safeReadSymbol(handle, "PQfinish");
  FARPROC procStatus = safeReadSymbol(handle, "PQstatus");
  FARPROC procErrMsg = safeReadSymbol(handle, "PQerrorMessage");
  FARPROC procQuery = safeReadSymbol(handle, "PQexec");
  FARPROC procRows = safeReadSymbol(handle, "PQntuples");
  FARPROC procCols = safeReadSymbol(handle, "PQnfields");
  FARPROC procField = safeReadSymbol(handle, "PQgetvalue");
  FARPROC procResStatus = safeReadSymbol(handle, "PQresultStatus");
  FARPROC procResErrMsg = safeReadSymbol(handle, "PQresultErrorMessage");
  FARPROC procResFinish = safeReadSymbol(handle, "PQclear");

  PQconnectdb = (tPQconnectdb*) procConnect;
  PQfinish = (tPQfinish*) procFinish;
  PQstatus = (tPQstatus*) procStatus;
  PQerrorMessage = (tPQerrorMessage*) procErrMsg;
  PQexec = (tPQexec*) procQuery;
  PQntuples = (tPQntuples*) procRows;
  PQnfields = (tPQnfields*) procCols;
  PQgetvalue = (tPQgetvalue*) procField;
  PQresultStatus = (tPQresultStatus*) procResStatus;
  PQresultErrorMessage = (tPQresultErrorMessage*) procResErrMsg;
  PQclear = (tPQclear*) procResFinish;
}

#endif

PGconn* PGLibraryLoader::connectdb(const AnsiString& conninfo) {
  return PQconnectdb(conninfo.c_str());
}
void PGLibraryLoader::finish(PGconn *conn) {
  PQfinish(conn);
}
ConnStatusType PGLibraryLoader::status(const PGconn *conn) {
  return PQstatus(conn);
}
AnsiString PGLibraryLoader::errorMessage(const PGconn *conn) {
  return PQerrorMessage(conn);
}
PGresult* PGLibraryLoader::exec(PGconn *conn, const AnsiString& query) {
  return PQexec(conn, query.c_str());
}
int PGLibraryLoader::ntuples(const PGresult *res) {
  return PQntuples(res);
}
int PGLibraryLoader::nfields(const PGresult *res) {
  return PQnfields(res);
}
AnsiString PGLibraryLoader::getvalue(const PGresult *res, int tup_num, int field_num) {
  const char* field = PQgetvalue(res, tup_num, field_num);
  if (field==0)
    return AnsiString();
  else
    return field;
}
ExecStatusType PGLibraryLoader::resultStatus(const PGresult *res) {
  return PQresultStatus(res);
}
AnsiString PGLibraryLoader::resultErrorMessage(const PGresult *res) {
  return PQresultErrorMessage(res);
}
void PGLibraryLoader::clear(PGresult *res) {
  return PQclear(res);
}

#else

#include "Exception.h"

PGLibraryLoader::PGLibraryLoader(const AnsiString&) {
}

PGconn* PGLibraryLoader::connectdb(const AnsiString& conninfo) {
  throw Exception("PGLibraryLoader::connectdb - compiled with NOPGSQL");
}
void PGLibraryLoader::finish(PGconn *conn) {
  throw Exception("PGLibraryLoader::finish - compiled with NOPGSQL");
}
ConnStatusType PGLibraryLoader::status(const PGconn *conn) {
  throw Exception("PGLibraryLoader::status - compiled with NOPGSQL");
}
AnsiString PGLibraryLoader::errorMessage(const PGconn *conn) {
  throw Exception("PGLibraryLoader::errorMessage - compiled with NOPGSQL");
}
PGresult* PGLibraryLoader::exec(PGconn *conn, const AnsiString& query) {
  throw Exception("PGLibraryLoader::exec - compiled with NOPGSQL");
}
int PGLibraryLoader::ntuples(const PGresult *res) {
  throw Exception("PGLibraryLoader::ntuples - compiled with NOPGSQL");
}
int PGLibraryLoader::nfields(const PGresult *res) {
  throw Exception("PGLibraryLoader::nfields - compiled with NOPGSQL");
}
AnsiString PGLibraryLoader::getvalue(const PGresult *res, int tup_num, int field_num) {
  throw Exception("PGLibraryLoader::getvalue - compiled with NOPGSQL");
}
ExecStatusType PGLibraryLoader::resultStatus(const PGresult *res) {
  throw Exception("PGLibraryLoader::resultStatus - compiled with NOPGSQL");
}
AnsiString PGLibraryLoader::resultErrorMessage(const PGresult *res) {
  throw Exception("PGLibraryLoader::resultErrorMessage - compiled with NOPGSQL");
}
void PGLibraryLoader::clear(PGresult *res) {
  throw Exception("PGLibraryLoader::clear - compiled with NOPGSQL");
}

#endif

PGLibraryLoader::~PGLibraryLoader() {
}





