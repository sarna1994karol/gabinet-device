//---------------------------------------------------------------------------

#ifndef DataBaseH
#define DataBaseH

#include "DBIntf.h"
#include "PGLibraryLoader.h"

class SDBConn : public DBIntf {
  PGLibraryLoader& pgLoader;
  PGconn* conn;
public:
  SDBConn(PGLibraryLoader&);
  virtual void Connect(const AnsiString&, const AnsiString&, const AnsiString&, const AnsiString&);
  virtual void Execute(const AnsiString&);
  virtual SDBResult Query(const AnsiString&);
  virtual void Disconnect();
  virtual AnsiString Escape(const AnsiString&);
  virtual ~SDBConn();
};

//---------------------------------------------------------------------------
#endif
