//---------------------------------------------------------------------------

#include "Database.h"
#include "StringUtil.h"

SDBConn::SDBConn(PGLibraryLoader& _pgLoader) : pgLoader(_pgLoader), conn(0) {
}

void SDBConn::Connect(const AnsiString& host, const AnsiString& user, const AnsiString& base, const AnsiString& passwd) {
  if (conn!=0)     
    Disconnect();
  AnsiString connStr = "dbname=" + base + " user=" + user + " port=5432 hostaddr=" + host;
  if (passwd.Length()>0)
    connStr += " password=" + passwd;
  conn = pgLoader.connectdb(connStr);
  if (conn==0)
    throw Exception("Nieznany b��d podczas pr�by po��czenia z baz� danych ["+connStr+"]");
  if (pgLoader.status(conn)!=CONNECTION_OK) {
    AnsiString err = pgLoader.errorMessage(conn);
    pgLoader.finish(conn);
    conn = 0;
    throw Exception("B��d podczas po��czenia z baz� danych ["+connStr+"] - "+err);
  }
}

void SDBConn::Execute(const AnsiString& query) {
  //printf("Q:[%s]\n", query.c_str());
  //logger.log("E:" + query);
  PGresult* res = pgLoader.exec(conn, query);
  if (res==0)
    throw Exception("Podczas po��czenia wystapi� nieznany b��d.");
  ExecStatusType status = pgLoader.resultStatus(res);
  if (status!=PGRES_EMPTY_QUERY && status!=PGRES_COMMAND_OK && status!=PGRES_TUPLES_OK) {
    AnsiString err = pgLoader.resultErrorMessage(res);
    pgLoader.clear(res);
    throw Exception("Podczas po��czenia wystapi� b��d ["+err+"].");
  }
  pgLoader.clear(res);
}

SDBResult SDBConn::Query(const AnsiString& query) {
  //printf("Q:[%s]\n", query.c_str());
  //logger.log("Q:" + query);
  PGresult* res = pgLoader.exec(conn, query);
  if (res==0)
    throw Exception("Podczas po��czenia wystapi� nieznany b��d.");
  ExecStatusType status = pgLoader.resultStatus(res);
  if (status!=PGRES_EMPTY_QUERY && status!=PGRES_COMMAND_OK && status!=PGRES_TUPLES_OK) {
    AnsiString err = pgLoader.resultErrorMessage(res);
    pgLoader.clear(res);
    throw Exception("Podczas po��czenia wystapi� b��d ["+err+"].");
  }
  SDBResult ret;                          
  int rows = pgLoader.ntuples(res);
  int cols = pgLoader.nfields(res);
  for (int i=0;i<rows;i++) {
    DynSet<AnsiString> row;
    for (int j=0;j<cols;j++)              
      row.Insert(pgLoader.getvalue(res, i, j));
    ret.Insert(row);
  }
  pgLoader.clear(res);
  return ret;
}

void SDBConn::Disconnect() {
  if (conn==0)
    return ;
  pgLoader.finish(conn);
  conn = 0;
}

AnsiString SDBConn::Escape(const AnsiString& s) {
  return Replace(Replace(s, "'", "''"), "\\", "\\\\");
}

SDBConn::~SDBConn() {
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
