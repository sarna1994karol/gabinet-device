#ifndef PANEL_UTILS_H
#define PANEL_UTILS_H

#include "data/AppData.h"
#include "DBIntf.h"
#include "Database.h"
#include "PGLibraryLoader.h"

class PanelUtils {
  public:
    static void update(Data&);
    static void execCmds(Data&);
};
#endif
