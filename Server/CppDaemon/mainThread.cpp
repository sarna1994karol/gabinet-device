#include <cstdio>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include "AnsiString.h"
#include "data/AppData.h"
#include "bashUtils.h"
#include "panelUtils.h"
#include "logger.h"

#define MAX_NET_CHECK 5

int main() {
  IfaceStatus empty(Bool::createFalse(), "", DhcpStatus::createEmpty(), Bool::createFalse());
  Data data = Data(NetStatus(empty, empty, empty, WifiList()));
  BashUtils::onStart();
  int lastNetCheck=MAX_NET_CHECK;
  while(true) {
    if(++lastNetCheck>=MAX_NET_CHECK) {
      BashUtils::updateNetStatus(data);
      lastNetCheck=0;
    }
    BashUtils::checkUsbDevices();
    //PanelUtils::update(data);
    //PanelUtils::execCmds(data);
    sleep(1);
  }
}
