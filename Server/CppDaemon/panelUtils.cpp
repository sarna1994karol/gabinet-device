#include "panelUtils.h"
#include "bashUtils.h"

#define TIMEOUT 5


void sqlExec(AnsiString query) {
//  printf("E: %s\n", query.c_str());
  AnsiString timestamp = "2016-02-04";
  PGLibraryLoader loader("libpq.dll");
  SDBConn db(loader);
  try {
    db.Connect("127.0.0.1", "postgres", "gabinet_dev", "xxx"); //dodac timeout!
    db.Execute(query);
  } catch (...) {
  }
  db.Disconnect();
  
}


SDBResult sqlQuery(AnsiString query) {
  SDBResult res;
  AnsiString timestamp = "2016-02-04";
  PGLibraryLoader loader("../../Baza/libpq.dll");
  SDBConn db(loader);
  try {
    db.Connect("127.0.0.1", "postgres", "gabinet_dev", "xxx"); //dodac timeout!
    res = db.Query(query);
  } catch (...) {
  }
  db.Disconnect();
  return res;
}

void updateProperty(AnsiString name, AnsiString value) {
  //printf("%s\n", ("UPDATE panel_DeviceInfo SET value='"+value+"' WHERE name='"+name+"'").c_str());
  sqlExec("UPDATE panel_DeviceInfo SET value='"+value+"' WHERE name='"+name+"'");
}

void updateIface(IfaceStatus& s, AnsiString name) {
  updateProperty(name+"_ip", s.getIpv4());

  if(s.getPlugIn().isTrue())
    updateProperty(name+"_plug", "Podlaczone!");
  else
    updateProperty(name+"_plug", "Nie podlaczone!");

  if(s.getDhcpStatus().isEmpty())
    updateProperty(name+"_dhcp", "Nieznany stan");
  else if(s.getDhcpStatus().isDhcpDetected())
    updateProperty(name+"_dhcp", "Wykryto serwer dhcp w sieci: " + s.getDhcpStatus().asDhcpDetected());
  else if(s.getDhcpStatus().isDhcpRun())
    updateProperty(name+"_dhcp", "Uruchomiono wlasny serwer dhcp");
  else
    updateProperty(name+"_dhcp", "Blad!");
}

void clearWifiList() {
}

void updateWifiList(WifiList& list) {
  AnsiString ts = "2016-02-05";
  for(int i=0;i<list.Size();i++) {
    AnsiString status;
    if(list[i].getConnected().isTrue())
      status = "polocznono";
    else
      status = "nie poloczono";
    sqlExec("INSERT INTO panel_WifiList (ssid, status, edit) SELECT '"+list[i].getSsid()+"','"+status+"','"+ts+"' WHERE NOT EXISTS (SELECT id FROM panel_Wifilist where ssid='"+list[i].getSsid()+"')");
  }
}

void PanelUtils::update(Data& data) {
  updateIface(data.getNetStatus().getEth(), "eth");
  updateIface(data.getNetStatus().getWlan(), "wlan");
  updateIface(data.getNetStatus().getLte(), "lte");
  updateWifiList(data.getNetStatus().getWifiList());
}

void PanelUtils::execCmds(Data& data) {
  return;
  SDBResult res = sqlQuery("SELECT id, name FROM panel_cmds WHERE status like 'waiting'");
  for(int i=0;i<res.Size();i++) {
    //printf("Cmd: %s\n", res[i][1].c_str());
    if (res[i][1]=="reboot") {
      sqlExec("UPDATE panel_cmds SET status='done' WHERE id="+res[i][0]);
      BashUtils::reboot();
    }
  }
}
