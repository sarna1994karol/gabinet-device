#include "logger.h"

void Logger::write(AnsiString file, AnsiString str) {
  FILE *fp;
  fp = fopen(file.c_str(), "a+");
  fprintf(fp, (str+"\n").c_str());
  fclose(fp);
}

void Logger::log(AnsiString str) {
  write(SERVICE_LOG, str);
}


