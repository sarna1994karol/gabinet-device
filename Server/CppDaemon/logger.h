#ifndef LOGGER
#define LOGGER

#include <cstdio>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include "AnsiString.h"
#include "DynSet.h"

#define SERVICE_LOG AnsiString("/var/log/gabinetService.log")

class Logger {
  public:
    static void write(AnsiString, AnsiString);
    static void log(AnsiString);
};

#endif
