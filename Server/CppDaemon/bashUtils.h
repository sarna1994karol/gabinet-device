#ifndef BASH_UTILS_H
#define BASH_UTILS_H

#include <cstdio>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <memory>

#include "data/AppData.h"

class BashUtils {

  private:
   
  public:
    static DynSet<AnsiString> exec(AnsiString cmd);
    static void updateNetStatus(Data&);
    static void updateSystemData(Data&);
    static void updateDeviceData(Data&);
    static void connectWifi(AnsiString, AnsiString);
    static void checkUsbDevices();
    static void onStart();
    static void reboot();
};

#endif
